import React, { Fragment } from 'react'
import 'antd/dist/antd.css'
import './index.css'
import { Button, Row, Col, Image, Table } from 'antd'

const { Column } = Table;
//relaiive fixed
const App = () => {
  return (
    <div>
      <Row gutter={20} className='scrollable-container' >
        <Col sm={4} md={6} lg={8} xl={12} className="parent">

          <Image id="img" className="preview"
            src='https://i.pinimg.com/236x/1a/7b/a9/1a7ba9a74fe5393e6b7ee4798cb79029--the-office-highlights.jpg?nii=t'
          />

        </Col>
        <Col sm={20} md={18} lg={16} xl={12}>
          <Fragment>
            <Table
              pagination={false}
              tableLayout="fixed"

            >
              <Column title="Биомаркер"
                dataIndex="biomarkerName"

                width="30%"
              />
              <Column title="Значение"
                dataIndex="value"
                align="center"

                width="20%"
              />
              <Column title="Единица измерения"
                dataIndex="unitBiomarkerName"
                align="center"
                width="18%"
              />
              <Column
                title="Норма"
                dataIndex="norm"
                align="center"

                width="15%"
              />

            </Table>
            <Button style={{ marginTop: "20px", width: "100%" }}
            >
              Добавить новое исследование
                                </Button>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
          </Fragment>
        </Col>
      </Row>

      <Row gutter={20} className='scrollable-container'>
        <Col sm={4} md={6} lg={8} xl={12} className="parent">
          <Image id="img" className="preview"
            src='https://i.pinimg.com/736x/8a/7e/a3/8a7ea37ebd68522ec58d9d8b6641bcda.jpg'
          />

        </Col>
        <Col sm={20} md={18} lg={16} xl={12}>
          <Fragment>
            <Table
              pagination={false}
              tableLayout="fixed"

            >
              <Column title="Биомаркер"
                dataIndex="biomarkerName"

                width="30%"
              />
              <Column title="Значение"
                dataIndex="value"
                align="center"

                width="20%"
              />
              <Column title="Единица измерения"
                dataIndex="unitBiomarkerName"
                align="center"
                width="18%"
              />
              <Column
                title="Норма"
                dataIndex="norm"
                align="center"

                width="15%"
              />

            </Table>
            <Button style={{ marginTop: "20px", width: "100%" }}
            >
              Добавить новое исследование
                                </Button>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
                                1<br></br>
          </Fragment>
        </Col>
      </Row>
    </div>

  )

}

export default App;
